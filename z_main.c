/**
  ******************************************************************************
  * @file    z_main.c
  * @author  Karatanov M.N.
  * @version V0.1
  * @date    17-June-2015
  * @brief   Function runs all threads
  ******************************************************************************  
  */


/* Includes ------------------------------------------------------------------*/
#include "utils.h"

/* Private functions ---------------------------------------------------------*/




/**
  * @brief  Main function
  * @param  None
  * @retval None
  */
void main( void )
{
    CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_1);    
  	
    tim.init();
    enableInterrupts();
	
	
    while(1)
    {
        
    }
}

