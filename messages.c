

#include <stdio.h>
#include "utils.h"


/* Global variables ---------------------------------------------------------*/
volatile static struct {
    u8 val;
    u8* paramPtr;
    u16  paramSize;
} messages[MAX_MSG];

/* Private functions ---------------------------------------------------------*/


/** @addtogroup MSG
  * @{
  */

/**
  * @brief  ������������� ���������
  * @param  None
  * @retval None
  */
static inline  void MSG_Init()
{
  for(u8 i = 0; i < MAX_MSG; i++)
	{
	  	messages[i].val = 0;
      messages[i].paramPtr = NULL;
      messages[i].paramSize = 0;
	}
}


/**
  * @brief  ���������� ��������� 
  * @param  None
  * @retval ������ ������
  */
static inline  u8 MSG_Set(const u8 imsg, void* const iparamPtr, const u16 iparamSize)
{
    if (imsg < MAX_MSG)
    {
        messages[imsg].val       = 1;
        messages[imsg].paramPtr  = (u8*)iparamPtr;
        messages[imsg].paramSize = iparamSize;
        return (u8)0;
    }
    return (u8)-1;
}



/**
  * @brief  �������� ��������� 
  * @param  ����� ��������� ���������
  * @retval ���� ��������� ��� ���
  */
static inline bool getMsgVal(const u8 imsg)
{
	if(2 == messages[imsg].val)
	{
	  	messages[imsg].val = 0;
	  	return true;
	}
	return false;
}
/**
  * @brief  �������� ��������� �� ��������� 
  * @param  ����� ��������� ���������
  * @retval ��������� 
  */
static inline void* getMsgPtr(const u8 imsg)
{
	return messages[imsg].paramPtr;
}
/**
  * @brief  �������� ������ ���������
  * @param  ����� ��������� ���������
  * @retval ������
  */
static inline u16 getMsgSize(const u8 imsg)
{
	return messages[imsg].paramSize;
}

/**
  * @brief  ������� �������������� ��������� 
  * @param  None
  * @retval None
  */
static inline void process_msg()
{
  static u8 i;
  
  ATOMIC_START();
    for (i = 0; i< MAX_MSG; i++) 
    {
      if (messages[i].val == 2) {messages[i].val = 0;}
      if (messages[i].val == 1) {messages[i].val = 2;}
    }
  ATOMIC_STOP();
}



/**
  * @brief  MSG_Driver.
*/
const struct Msg_driver msg = 
{
	.init 	 = MSG_Init,
	.process = process_msg,
  .set     = MSG_Set,
	.getVal	 = getMsgVal,
	.getPtr  = getMsgPtr,
	.getSize = getMsgSize,
};

/**
  * @}
  */

