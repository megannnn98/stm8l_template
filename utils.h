

#include  "stm8l15x.h"
#include <stdbool.h>
#include "pt.h"
#include "pt-sem.h"

enum
{
  GENERAL_TIMER  		= 0,
  MAX_TIMERS				= 1,
};

enum
{
  GENERAL_MSG  			  = 0,
  MAX_MSG    				  = 1,
};

#define ATOMIC_START() do { disableInterrupts()
#define ATOMIC_STOP()  enableInterrupts(); } while(0)

#define PT_DELAY_MS(timer, ms)	do { tim.reset(timer); \
								 PT_WAIT_WHILE(pt, tim.get(timer) < ms); } while(0)

#define PT_STATE_INIT() 	 static pt_t val_pt = {.lc = 0}; \
							 static pt_t *pt = &val_pt;


/* Global variables ---------------------------------------------------------*/


/** @defgroup Timer_driver
  * @{
  */
struct Timer_driver
{
	uint32_t (*get)(uint8_t timer);
	void (*reset)(uint8_t timer);
	void (*init)();
	void (*set)(uint8_t timer, uint32_t val);
	
#ifdef USE_DISPATCHER
	void (*RTOS_dispatch)();
	void (*RTOS_SetTask)(void (*taskFunc)(void), uint32_t taskDelay, uint32_t taskPeriod);
	void (*RTOS_DeleteTask)(void (*taskFunc)(void));
	void (*RTOS_Reset)();
	unsigned char (*RTOS_GetNumTasks)();
    
#endif 
};
/**
  * @}
  */
extern const struct Timer_driver tim;



/**
  * @{ @brief  Структура дравера MSG.
*/
struct Msg_driver
{
	void (*init)();
    void (*process)(); 
    u8   (*set)(const u8 imsg, void* iparamPtr, u16 iparamSize);
	bool (*getVal)(const u8 imsg)	 ;
	void* (*getPtr)(const u8 imsg) ;
	u16   (*getSize)(const u8 imsg);
};
/**
  * @}
  */

extern const struct Msg_driver msg;

