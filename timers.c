
#include "utils.h"


/**
  ******************************************************************************
  * @file    timers.c
  * @author  Karatanov
  * @version V1.0.0
  * @date    05-February-2016
  * @brief   Timer functions
  */


#ifdef USE_DISPATCHER
#include "dispatcher.h"
#endif
/* Global variables ---------------------------------------------------------*/


/* Private variables ---------------------------------------------------------*/
static uint32_t timers[MAX_TIMERS];



#ifdef USE_DISPATCHER
volatile uint8_t arrayTail;
volatile task TaskArray[MAX_TASKS];      // ������� �����
#endif


/** @addtogroup Timer Control
  * @{
  */

/* Private functions ---------------------------------------------------------*/



/**
  * @brief  ������������� ���������� ������� 1��
  * @retval None
  */
static void osTimerInit()
{
  	for(u8 i = 0; i < MAX_TIMERS; i++)
	{
	  	timers[i] = 0;
	}
#ifdef USE_DISPATCHER
	for (uint8_t i = 0; i<arrayTail; i++)
	{
		if  (TaskArray[i].delay == 0) {
		  	TaskArray[i].run = 1;
		}
		else {
		  TaskArray[i].delay--; 
		}
	}

	arrayTail = 0;
#endif
	CLK_LSICmd(ENABLE);
	while(RESET == CLK_GetFlagStatus(CLK_FLAG_LSIRDY));
	
	CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
	CLK_RTCClockConfig(CLK_RTCCLKSource_LSI , CLK_RTCCLKDiv_2);

	RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div8); 
	RTC_ITConfig(RTC_IT_WUT, ENABLE);
	RTC_SetWakeUpCounter(1);
    RTC_WakeUpCmd(ENABLE);
}


/**
  * @brief  ����� ������� 
  * @param  ����� ������� � �������
  * @retval None
  */
static void resetTimer(const uint8_t timer)
{
  	timers[timer] = 0;
}

/**
  * @brief  ��������� ������� � �������� 
  * @param  ����� ������� � �������
  * @param  ����� �������� 
  * @retval None
  */
static void setTimer(const uint8_t timer, const uint32_t val)
{
  	timers[timer] = val;
}

/**
  * @brief  �������� �������� �������
  * @param  ����� ������� � �������
  * @retval ������� �������� �������
  */
static uint32_t getTimer(const uint8_t timer)
{
  	return timers[timer];
}

#ifdef USE_DISPATCHER


/**
  * @brief  �������� ���� ������� �� ������� 
  * @param  None 
  * @retval None 
  */
static void RTOS_Reset()
{
	arrayTail = 0;
}

/**
  * @brief  �������� ���������� ������� �������
  * @param  None 
  * @retval ���������� ������� � ������� 
  */
static unsigned char RTOS_GetNumTasks()
{
    return arrayTail;
}


/**
  * @brief  ������� ������� 
  * @param  ��������� �� ������� 
  * @param  �������� ����� ����������� ( 0 - ����������� ��� ��������)
  * @param  ������� ������( 0 - ����������� �����)
  * @retval ���������� ������� � ������� 
  */
static void RTOS_SetTask (void (* const taskFunc)(void), const uint32_t taskDelay, const uint32_t taskPeriod)
{
   if(!taskFunc) return;
   for(uint8_t i = 0; i < arrayTail; i++)
   {
      if(TaskArray[i].pFunc == taskFunc)
      {
	  	 //disableInterrupts();

         TaskArray[i].delay  = taskDelay;
         TaskArray[i].period = taskPeriod;
         TaskArray[i].run    = 0;   

	  	 //enableInterrupts();
		 return;
      }
   }

   if (arrayTail < MAX_TASKS)
   {
	  //disableInterrupts();
      
      TaskArray[arrayTail].pFunc  = taskFunc;
      TaskArray[arrayTail].delay  = taskDelay;
      TaskArray[arrayTail].period = taskPeriod;
      TaskArray[arrayTail].run    = 0;   

      arrayTail++;
	  //enableInterrupts();
   }
}

/**
  * @brief  ������� �������
  * @param  ��������� �� ������� 
  * @retval None
  */
static void RTOS_DeleteTask (void (* const taskFunc)(void))
{
   for (uint8_t i = 0; i<arrayTail; i++)
   {
      if(TaskArray[i].pFunc == taskFunc)
      {
         
	  	 //disableInterrupts();
         if(i != (arrayTail - 1))
         {
            TaskArray[i] = TaskArray[arrayTail - 1];
         }
         arrayTail--;
	  	 //enableInterrupts();
		 return;
      }
   }
}

/**
  * @brief  ��������� �������, ��� ������� ��������� � while(1)
  * @param  None
  * @retval None
  */
static void RTOS_DispatchTask()
{
	void (*function) (void);
	for (uint8_t i = 0; i<arrayTail; i++)
	{
		if (1 == TaskArray[i].run)
		{

			function = TaskArray[i].pFunc;
			if(0 == TaskArray[i].period)
			{
				RTOS_DeleteTask(TaskArray[i].pFunc);
			}
			else
			{
				TaskArray[i].run = 0;
				if(!TaskArray[i].delay)
				{
					TaskArray[i].delay = TaskArray[i].period-1; 
				}
			}
			(*function)();
		}
	}
}

#endif


/**
  * @}
  */



/**
  * @brief  Timer_Driver.
*/
const struct Timer_driver tim = 
{
	.get 	= getTimer,
	.reset  = resetTimer,
	.init   = osTimerInit,
	.set	= setTimer,
    
#ifdef USE_DISPATCHER    
	.RTOS_dispatch = RTOS_DispatchTask,
	.RTOS_SetTask = RTOS_SetTask,
	.RTOS_DeleteTask = RTOS_DeleteTask,
	.RTOS_Reset = RTOS_Reset,
	.RTOS_GetNumTasks = RTOS_GetNumTasks,
#endif
};

